<?php

namespace App\Tests\Api\Product;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GetProductsTest extends WebTestCase
{
    public function testThatGetProductsReturnsOK()
    {
        $client = static::createClient();
        $client->request('GET', '/products');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testThatGetProductsReturnsExpectedProducts()
    {
        $client = static::createClient();
        $client->request('GET', '/products');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertCount(5, json_decode($client->getResponse()->getContent(), true));
    }

    public function testThatInvalidParameterPriceLessReturnsBadRequest()
    {
        $client = static::createClient();
        $client->request('GET', '/products?price_less_than=dsfsf');
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testThatProductsAreFilteredByCategory()
    {
        $client = static::createClient();
        $client->request('GET', '/products?category=boots');

        $products = json_decode($client->getResponse()->getContent(), true);

        foreach ($products as $product) {
            $this->assertEquals('boots', $product['category']);
        }
    }

    public function testThatProductsAreFilteredByPriceLessThan()
    {
        $client = static::createClient();
        $client->request('GET', '/products?price_less_than=80000');

        $products = json_decode($client->getResponse()->getContent(), true);

        foreach ($products as $product) {
            $this->assertLessThanOrEqual(80000, $product['price']['original']);
        }
    }

    public function testThatProductsAreFilteredByPriceLessThanAndCategory()
    {
        $client = static::createClient();
        $client->request('GET', '/products?price_less_than=80000&category=boots');

        $products = json_decode($client->getResponse()->getContent(), true);

        foreach ($products as $product) {
            $this->assertLessThanOrEqual(80000, $product['price']['original']);
            $this->assertEquals('boots', $product['category']);
        }
    }
}
