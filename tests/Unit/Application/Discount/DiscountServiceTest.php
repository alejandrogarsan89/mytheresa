<?php

namespace App\Tests\Unit\Application\Discount;

use App\Domain\Discount\DiscountService;
use App\Domain\Product\Category;
use App\Domain\Product\Name;
use App\Domain\Product\Product;
use App\Domain\Product\Sku;
use App\Infrastructure\Repository\Discount\InMemoryCategoryDiscountRepository;
use App\Infrastructure\Repository\Discount\InMemorySkuDiscountRepository;
use Money\Currency;
use Money\Money;
use PHPUnit\Framework\TestCase;

class DiscountServiceTest extends TestCase
{
    private DiscountService $discountService;

    public function setUp()
    {
        $this->discountService = new DiscountService(
            new InMemoryCategoryDiscountRepository(),
            new InMemorySkuDiscountRepository()
        );
    }

    public function testThatDiscountByCategoryIsApplied()
    {
        $product = $this->discountService->attachDiscount(
            new Product(
                new Name('BV Lean leather ankle boots'),
                new Sku('000002'),
                new Category('boots'),
                new Money(89000, new Currency('EUR'))
            ),
        );

        $this->assertNotNull($product->getDiscount());
        $this->assertEquals(30, $product->getDiscount()->getPercentage());
    }

    public function testThatDiscountBySkuIsApplied()
    {
        $product = $this->discountService->attachDiscount(
            new Product(
                new Name('BV Lean leather ankle boots'),
                new Sku('000003'),
                new Category('nnn'),
                new Money(89000, new Currency('EUR'))
            ),
        );

        $this->assertNotNull($product->getDiscount());
        $this->assertEquals(15, $product->getDiscount()->getPercentage());
    }

    public function testThatMoreRestrictiveDiscountIsApplied()
    {
        $product = $this->discountService->attachDiscount(
            new Product(
                new Name('BV Lean leather ankle boots'),
                new Sku('000003'),
                new Category('boots'),
                new Money(89000, new Currency('EUR'))
            ),
        );

        $this->assertNotNull($product->getDiscount());
        $this->assertEquals(30, $product->getDiscount()->getPercentage());
    }
}
