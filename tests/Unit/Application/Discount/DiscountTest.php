<?php

namespace App\Tests\Unit\Application\Discount;

use App\Domain\Discount\Discount;
use Money\Currency;
use Money\Money;
use PHPUnit\Framework\TestCase;

class DiscountTest extends TestCase
{
    public function testDiscountApply()
    {
        $discount = new Discount(30);
        $newPrice = $discount->apply(new Money('100', new Currency('EUR')));
        $this->assertEquals(70, $newPrice->getAmount());
    }

    /**
     * @dataProvider isMoreRestrictiveProvider
     * @param Discount $discountA
     * @param Discount $discountB
     * @param bool $expectedResult
     */
    public function testIsMoreRestrictive(Discount $discountA, Discount $discountB, bool $expectedResult)
    {
        $this->assertEquals($expectedResult, $discountA->isMoreRestrictive($discountB));
    }

    /**
     * @dataProvider invalidDiscountValuesProvider
     * @param int $value
     */
    public function testThatDiscountValueOutOfValidRangeThrowsException(int $value)
    {
        $this->expectException(\InvalidArgumentException::class);
        new Discount($value);
    }

    public function invalidDiscountValuesProvider()
    {
        return [
            [-1],
            [101]
        ];
    }

    public function isMoreRestrictiveProvider()
    {
        return [
            [new Discount(30), new Discount(20), true],
            [new Discount(20), new Discount(21), false],
            [new Discount(30), new Discount(30), false]
        ];
    }
}
