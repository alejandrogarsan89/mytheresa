<?php

namespace App\Tests\Unit\Application\Product;

use App\Application\Product\GetProducts;
use App\Application\Product\GetProductsRequest;
use App\Domain\Discount\DiscountService;
use App\Infrastructure\Repository\Discount\InMemoryCategoryDiscountRepository;
use App\Infrastructure\Repository\Discount\InMemorySkuDiscountRepository;
use App\Infrastructure\Repository\Product\InMemoryProductRepository;
use PHPUnit\Framework\TestCase;

class GetProductsTest extends TestCase
{
    private GetProducts $getProducts;

    public function setUp()
    {
        $this->getProducts = new GetProducts(
            new InMemoryProductRepository(InMemoryProductRepository::defaultProducts()),
            new DiscountService(
                new InMemoryCategoryDiscountRepository(),
                new InMemorySkuDiscountRepository()
            )
        );
    }

    public function testThatFiltersAreNotAppliedIfNotSpecifiedInRequest()
    {
        $products = $this->getProducts->execute(new GetProductsRequest(
            null,
            null
        ));

        $this->assertCount(5, $products->jsonSerialize());
    }

    public function testThatProductsAreFilteredByCategory()
    {
        $products = $this->getProducts->execute(new GetProductsRequest(
            'boots',
            null
        ));

        $this->assertCount(3, $products->jsonSerialize());
        $this->assertEquals('boots', $products->jsonSerialize()[0]['category']);
    }

    public function priceFilterProvider()
    {
        return [
            [1000, 0],
            [59000, 1]
        ];
    }

    /**
     * @dataProvider priceFilterProvider
     * @param int $price
     * @param int $expectedNumberOfProducts
     */
    public function testThatProductsAreFilteredByPrice(int $price, int $expectedNumberOfProducts)
    {
        $products = $this->getProducts->execute(new GetProductsRequest(
            null,
            $price
        ));

        $this->assertCount($expectedNumberOfProducts, $products->jsonSerialize());
    }

    public function testThatInvalidPriceFormatThrowsException()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->getProducts->execute(new GetProductsRequest(
            null,
            "33453456v"
        ));
    }

    public function testThatResultContainsExpectedData()
    {
        $products = $this->getProducts->execute(new GetProductsRequest(
            null,
            null
        ));

        foreach ($products->jsonSerialize() as $product) {
            $this->assertArrayHasKey('sku', $product);
            $this->assertArrayHasKey('category', $product);
            $this->assertArrayHasKey('price', $product);
            $this->assertArrayHasKey('original', $product['price']);
            $this->assertArrayHasKey('final', $product['price']);
            $this->assertArrayHasKey('discount_percentage', $product['price']);
            $this->assertArrayHasKey('currency', $product['price']);
            $this->assertArrayHasKey('name', $product);
        }
    }
}
