PROJECT_NAME = mytheresa-app

.PHONY: up
up:
	docker-compose build
	docker-compose up -d --remove-orphans
	docker exec -it '$(PROJECT_NAME)' composer install --no-interaction
	docker exec -it '$(PROJECT_NAME)' cp .env .env.local

.PHONY: bash
bash:
	docker exec -it '$(PROJECT_NAME)' /bin/bash

.PHONY: build
build: up

.PHONY: down
down:
	docker-compose down

.PHONY: clean
clean:
	docker-compose down -v

.PHONY: cache
cache:
	docker exec -u $(shell id -u) '$(PROJECT_NAME)' bin/console cache:clear --env=dev
	docker exec -u $(shell id -u) '$(PROJECT_NAME)' bin/console cache:clear --env=test
	docker exec -u $(shell id -u) '$(PROJECT_NAME)' bin/console cache:clear --env=prod

.PHONY: test
test:
	docker exec '$(PROJECT_NAME)' ./vendor/bin/phpunit -v
