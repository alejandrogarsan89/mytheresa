<?php

declare(strict_types=1);

namespace App\Application\Product;

final class GetProductsRequest
{
    private ?string $category;
    private ?string $priceLessThan;

    public function __construct(?string $category, ?string $priceLessThan)
    {
        $this->category = $category;
        $this->priceLessThan = $priceLessThan;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function getPriceLessThan(): ?string
    {
        return $this->priceLessThan;
    }
}
