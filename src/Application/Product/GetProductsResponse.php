<?php

declare(strict_types=1);

namespace App\Application\Product;

use App\Domain\Product\Product;

final class GetProductsResponse implements \JsonSerializable
{
    private array $products;

    public function __construct(array $products)
    {
        $this->products = $products;
    }

    public function jsonSerialize()
    {
        return array_map(
            fn(Product $product) => [
                'sku' => $product->getSku()->getValue(),
                'name' => $product->getName()->getValue(),
                'category' => $product->getCategory()->getValue(),
                'price' => [
                    'original' => $product->getOriginalPrice()->getAmount(),
                    'final' => $product->getFinalPrice()->getAmount(),
                    'discount_percentage' => $product->getDiscount(),
                    'currency' => $product->getOriginalPrice()->getCurrency()->getCode()
                ]
            ],
            $this->products
        );
    }
}
