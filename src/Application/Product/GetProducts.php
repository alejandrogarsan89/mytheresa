<?php

declare(strict_types=1);

namespace App\Application\Product;

use App\Domain\Discount\DiscountService;
use App\Domain\Product\Category;
use App\Domain\Product\Product;
use App\Domain\Product\ProductFilter;
use App\Domain\Product\ProductRepository;
use Money\Currency;
use Money\Money;

final class GetProducts
{
    private ProductRepository $productRepository;
    private DiscountService $discountService;

    public function __construct(
        ProductRepository $productRepository,
        DiscountService $discountService
    )
    {
        $this->productRepository = $productRepository;
        $this->discountService = $discountService;
    }

    public function execute(GetProductsRequest $getProductsRequest): GetProductsResponse
    {
        $productFilter = new ProductFilter();

        if($getProductsRequest->getCategory()) {
            $productFilter->withCategory(new Category($getProductsRequest->getCategory()));
        }

        if($getProductsRequest->getPriceLessThan()) {
            $productFilter->withPriceLessThan(new Money(
                $getProductsRequest->getPriceLessThan(),
                new Currency('EUR'))
            );
        }

        $products = $this->productRepository->All($productFilter);

        return new GetProductsResponse(array_map(
            fn(Product $product) => $this->discountService->attachDiscount($product),
            $products
        ));
    }
}
