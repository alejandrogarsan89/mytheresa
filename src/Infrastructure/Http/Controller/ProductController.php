<?php

declare(strict_types=1);

namespace App\Infrastructure\Http\Controller;

use App\Application\Product\GetProducts;
use App\Application\Product\GetProductsRequest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

final class ProductController
{
    private GetProducts $getProducts;

    public function __construct(GetProducts $getProducts)
    {
        $this->getProducts = $getProducts;
    }

    public function getProducts(Request $request): JsonResponse
    {
        try {
            $products = $this->getProducts->execute(new GetProductsRequest(
                $request->get('category', null),
                $request->get('price_less_than', null)
            ));

            return new JsonResponse(
                $products,
                JsonResponse::HTTP_OK
            );
        }
        catch (\InvalidArgumentException $exception) {
            return new JsonResponse(
                ['message' => $exception->getMessage()],
                JsonResponse::HTTP_BAD_REQUEST
            );
        } catch (\Exception $exception) {
            return new JsonResponse(
                ['message' => $exception->getMessage()],
                JsonResponse::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }
}
