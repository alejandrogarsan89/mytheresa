<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Discount;

use App\Domain\Discount\Discount;
use App\Domain\Discount\SkuDiscount;
use App\Domain\Discount\SkuDiscountRepository;
use App\Domain\Product\Sku;

final class InMemorySkuDiscountRepository implements SkuDiscountRepository
{
    private array $skuDiscounts;

    public function __construct()
    {
        $this->skuDiscounts = [
            new SkuDiscount(new Sku("000003"), new Discount(15))
        ];
    }
    public function bySku(Sku $sku): ?SkuDiscount
    {
        $foundSkuDiscount = null;

        foreach ($this->skuDiscounts as $skuDiscount) {
            if ($skuDiscount->getSku()->equals($sku)) {
                return $skuDiscount;
            }
        }

        return null;
    }
}
