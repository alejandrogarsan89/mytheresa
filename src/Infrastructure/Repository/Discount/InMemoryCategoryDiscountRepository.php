<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Discount;

use App\Domain\Discount\CategoryDiscount;
use App\Domain\Discount\CategoryDiscountRepository;
use App\Domain\Discount\Discount;
use App\Domain\Product\Category;

final class InMemoryCategoryDiscountRepository implements CategoryDiscountRepository
{
    private array $categoryDiscounts;

    public function __construct()
    {
        $this->categoryDiscounts = [
            new CategoryDiscount(new Category("boots"), new Discount(30))
        ];
    }

    public function byCategory(Category $category): ?CategoryDiscount
    {
        $foundCategoryDiscount = null;

        foreach ($this->categoryDiscounts as $categoryDiscount) {
            if ($categoryDiscount->getCategory()->equals($category)) {
                return $categoryDiscount;
            }
        }

        return null;
    }
}
