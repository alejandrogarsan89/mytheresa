<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository\Product;

use App\Domain\Product\Category;
use App\Domain\Product\Name;
use App\Domain\Product\Product;
use App\Domain\Product\ProductFilter;
use App\Domain\Product\ProductRepository;
use App\Domain\Product\Sku;
use Money\Currency;
use Money\Money;

final class InMemoryProductRepository implements ProductRepository
{
    private array $products;

    public function __construct(array $products = [])
    {
        $this->products = $products;

        if (empty($products)) {
            $this->products = self::defaultProducts();
        }
    }

    public static function defaultProducts()
    {
        return [
            new Product(
                new Name('BV Lean leather ankle boots'),
                new Sku('000001'),
                new Category('boots'),
                new Money(89000, new Currency('EUR'))
            ),
            new Product(
                new Name('BV Lean leather ankle boots'),
                new Sku('000002'),
                new Category('boots'),
                new Money(99000, new Currency('EUR'))
            ),
            new Product(
                new Name('Ashlington leather ankle boots'),
                new Sku('000003'),
                new Category('boots'),
                new Money(71000, new Currency('EUR'))
            ),
            new Product(
                new Name('Naima embellished suede sandals'),
                new Sku('000004'),
                new Category('sandals'),
                new Money(79500, new Currency('EUR'))
            ),
            new Product(
                new Name('Nathane leather sneakers'),
                new Sku('000005'),
                new Category('sneakers'),
                new Money(59000, new Currency('EUR'))
            )
        ];
    }

    public function All(ProductFilter $productFilter): array
    {
        return array_filter($this->products, function (Product $product) use ($productFilter): bool {
            if (
                $productFilter->getCategory() &&
                !$productFilter->getCategory()->equals($product->getCategory())
            ) {
                return false;
            }

            if (
                $productFilter->getPriceLessThan() &&
                !$productFilter->getPriceLessThan()->greaterThanOrEqual($product->getOriginalPrice())
            ) {
                return false;
            }

            return true;
        });
    }
}
