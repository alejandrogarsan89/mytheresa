<?php

declare(strict_types=1);

namespace App\Domain\Product;

class Category
{
    private string $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function equals(Category $category): bool
    {
        return $this->value === $category->getValue();
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
