<?php

declare(strict_types=1);

namespace App\Domain\Product;

class Sku
{
    private string $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function equals(Sku $sku)
    {
        return $this->value === $sku->getValue();
    }
}
