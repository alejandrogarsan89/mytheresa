<?php

declare(strict_types=1);

namespace App\Domain\Product;

interface ProductRepository
{
    public function All(ProductFilter $productFilter): array;
}
