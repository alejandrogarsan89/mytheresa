<?php

declare(strict_types=1);

namespace App\Domain\Product;

use Money\Money;

final class ProductFilter
{
    private ?Category $category = null;
    private ?Money $priceLessThan = null;

    public function withCategory(Category $category): ProductFilter
    {
        $this->category = $category;
        return $this;
    }

    public function withPriceLessThan(Money $price): ProductFilter
    {
        $this->priceLessThan = $price;
        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function getPriceLessThan(): ?Money
    {
        return $this->priceLessThan;
    }
}
