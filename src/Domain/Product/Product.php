<?php

declare(strict_types=1);

namespace App\Domain\Product;

use App\Domain\Discount\Discount;
use Money\Money;

final class Product
{
    private Name $name;
    private Sku $sku;
    private Category $category;
    private Money $price;
    private ?Discount $discount = null;

    public function __construct(Name $name, Sku $sku, Category $category, Money $price)
    {
        $this->name = $name;
        $this->sku = $sku;
        $this->category = $category;
        $this->price = $price;
    }

    public function attachDiscount(Discount $discount)
    {
        $this->discount = $discount;
    }

    public function getDiscount(): ?Discount
    {
        return $this->discount;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function getSku(): Sku
    {
        return $this->sku;
    }

    public function getCategory(): Category
    {
        return $this->category;
    }

    public function getOriginalPrice(): Money
    {
        return $this->price;
    }

    public function getFinalPrice(): Money
    {
        return $this->discount === null ? $this->price: $this->discount->apply($this->price);
    }
}
