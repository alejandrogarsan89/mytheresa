<?php

declare(strict_types=1);

namespace App\Domain\Discount;

use Money\Money;

final class Discount
{
    private int $percentage;

    public function __construct(int $percentage)
    {
        if ($percentage < 0 || $percentage > 100) {
            throw new \InvalidArgumentException("percentage must be between 0 and 100");
        }

        $this->percentage = $percentage;
    }

    public function apply(Money $money): Money
    {
        return new Money(
            round($money->getAmount() - $money->getAmount() * ($this->percentage/100)),
            $money->getCurrency()
        );
    }

    public function getPercentage(): int
    {
        return $this->percentage;
    }

    public function isMoreRestrictive(Discount $discount): bool
    {
        return $this->percentage > $discount->getPercentage();
    }
}
