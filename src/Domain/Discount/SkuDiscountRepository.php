<?php

declare(strict_types=1);

namespace App\Domain\Discount;

use App\Domain\Product\Sku;

interface SkuDiscountRepository
{
    public function bySku(Sku $sku): ?SkuDiscount;
}
