<?php

declare(strict_types=1);

namespace App\Domain\Discount;

interface Discountable
{
    public function getDiscount(): Discount;
}
