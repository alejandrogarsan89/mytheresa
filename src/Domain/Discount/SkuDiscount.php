<?php

declare(strict_types=1);

namespace App\Domain\Discount;

use App\Domain\Product\Sku;

final class SkuDiscount implements Discountable
{
    private Sku $sku;
    private Discount $discount;

    public function __construct(Sku $sku, Discount $discount)
    {
        $this->sku = $sku;
        $this->discount = $discount;
    }

    public function getSku(): Sku
    {
        return $this->sku;
    }

    public function getDiscount(): Discount
    {
        return $this->discount;
    }
}
