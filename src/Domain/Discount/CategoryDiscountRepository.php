<?php

declare(strict_types=1);

namespace App\Domain\Discount;

use App\Domain\Product\Category;

interface CategoryDiscountRepository
{
    public function byCategory(Category $category): ?CategoryDiscount;
}
