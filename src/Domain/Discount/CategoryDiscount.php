<?php

declare(strict_types=1);

namespace App\Domain\Discount;

use App\Domain\Product\Category;

final class CategoryDiscount implements Discountable
{
    private Category $category;
    private Discount $discount;

    public function __construct(Category $category, Discount $discount)
    {
        $this->category = $category;
        $this->discount = $discount;
    }

    public function getCategory(): Category
    {
        return $this->category;
    }

    public function getDiscount(): Discount
    {
        return $this->discount;
    }
}
