<?php

declare(strict_types=1);

namespace App\Domain\Discount;

use App\Domain\Product\Product;

final class DiscountService
{
    private CategoryDiscountRepository $categoryDiscountRepository;
    private SkuDiscountRepository $skuDiscountRepository;

    public function __construct(
        CategoryDiscountRepository $categoryDiscountRepository,
        SkuDiscountRepository $skuDiscountRepository
    )
    {
        $this->categoryDiscountRepository = $categoryDiscountRepository;
        $this->skuDiscountRepository = $skuDiscountRepository;
    }

    public function attachDiscount(Product $product): Product
    {
        $categoryDiscount = $this->categoryDiscountRepository->byCategory($product->getCategory());
        $skuDiscount = $this->skuDiscountRepository->bySku($product->getSku());

        $discountable = array_filter([$categoryDiscount, $skuDiscount]);
        $discounts = array_map(fn(Discountable $discountable) => $discountable->getDiscount(), $discountable);
        usort(
            $discounts,
            fn(Discount $discountA, Discount $discountB) => !$discountA->isMoreRestrictive($discountB)
        );

        if (array_key_exists(0, $discounts)) {
            $product->attachDiscount($discounts[0]);
        }

        return $product;
    }
}
